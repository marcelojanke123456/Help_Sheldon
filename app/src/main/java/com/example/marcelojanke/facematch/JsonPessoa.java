package com.example.marcelojanke.facematch;

import android.content.Context;

import android.util.Log;

import com.example.marcelojanke.facematch.WebRequest.WebInit;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;


public class JsonPessoa {

    private PessoaObject pessoaObject;
    private Context context;

    JsonPessoa(PessoaObject pessoaObject, Context context) {
        this.pessoaObject = pessoaObject;
        this.context = context;
    }

    public void ConvertAndSend() throws JSONException {
        JSONObject object = new JSONObject();
        object.put("Nome", this.pessoaObject.getNome());
        object.put("Idade", this.pessoaObject.getIdade());
        object.put("Descricao", this.pessoaObject.getDescricao());
        object.put("Turma", this.pessoaObject.getTurma());

        ArrayList Ola = pessoaObject.getInterresse();

        JSONArray array = new JSONArray();

        for (Object i : Ola) {
            array.put(i);
        }


        object.put("Interresse", array);


        Log.d("Naim", object.toString());


        WebInit init = new WebInit(this.context, "OP=4");



        init.Send(object);
        init.getIDUser(pessoaObject.getNome());



    }



}
