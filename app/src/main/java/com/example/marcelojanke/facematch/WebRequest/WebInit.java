package com.example.marcelojanke.facematch.WebRequest;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.ArrayList;

public class WebInit {
    private final Context context;
    private String URL;
    private final String coreIP;



    public WebInit(Context context, String URL) {
        this.context = context;
        this.URL = URL;
        this.coreIP = "http://helpsheldon-com.umbler.net/Main.php?";
    }


    public void Info(){

        RequestQueue requestQueue = Volley.newRequestQueue(this.context.getApplicationContext());
        StringRequest request = new StringRequest(Request.Method.GET, this.coreIP+this.URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Naim",response);
                        Toast.makeText(context,response,Toast.LENGTH_LONG).show();


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context,error.toString(),Toast.LENGTH_LONG).show();


            }
        });

        requestQueue.add(request);
    }

    public void Login(){
        RequestQueue queue = Volley.newRequestQueue(this.context);
        StringRequest request = new StringRequest(Request.Method.GET,this.coreIP+this.URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(context,response,Toast.LENGTH_SHORT).show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context,error.toString(),Toast.LENGTH_SHORT).show();

                Log.d("AHA",error.toString());
            }
        });

        queue.add(request);

    }

    public ArrayList GetQueue(){

        return null;
    }

    public void Send(JSONObject object){



    }

    public void getIDUser(String UserName){
        this.URL += UserName;

        final String[] Returned = new String[1];

        RequestQueue queue = Volley.newRequestQueue(this.context);

        StringRequest request = new StringRequest(Request.Method.GET, this.coreIP + this.URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(context,response,Toast.LENGTH_SHORT).show();

                        SqLiteCreator creator = new SqLiteCreator(context);
                        creator.Insert(response);

                        creator.close();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context,error.toString(),Toast.LENGTH_SHORT).show();
                Returned[0] = null;
            }
        });

        queue.add(request);
    }

}
