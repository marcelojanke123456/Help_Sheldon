package com.example.marcelojanke.facematch;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.marcelojanke.facematch.WebRequest.WebInit;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FloatingActionButton Like = findViewById(R.id.Like);

        Like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Action(1);
            }
        });

        FloatingActionButton Unlike = findViewById(R.id.Unlike);

        Unlike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Action(2);
            }
        });



    }

    private void Action(int Action){
        WebInit init =
                new WebInit(getApplicationContext(),
                        "OP="+Action);
        init.Info();
    }

}
