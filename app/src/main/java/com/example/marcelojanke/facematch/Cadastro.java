package com.example.marcelojanke.facematch;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.marcelojanke.facematch.Model.PessoaObject;

import org.json.JSONException;


import java.util.ArrayList;


public class Cadastro extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);
    }

    public void Cadastrar(View v){

        final EditText nome = findViewById(R.id.txtNome);
        final EditText idade =  findViewById(R.id.txtIdade);

        final RadioGroup rg = findViewById(R.id.rbg);
        final RadioButton generoSelecionado = findViewById(rg.getCheckedRadioButtonId());


        final CheckBox cbHomem = findViewById(R.id.cbHomens);
        final CheckBox cbMulheres = findViewById(R.id.cbMulheres);

        final EditText turma = findViewById(R.id.txtTurma);


        final EditText descricao = findViewById(R.id.txtDescricao);


        Button Cadastro = findViewById(R.id.btnCadastro);

        Cadastro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String Nome = nome.getText().toString();
                String Idade = idade.getText().toString();

                String Descricao = descricao.getText().toString();
                String Turma = turma.getText().toString();
                ArrayList<String> Interresse = new ArrayList<>();

                if (cbHomem.isChecked()){
                    Interresse.add("Homens");
                }

                if(cbMulheres.isChecked()){
                    Interresse.add("Mulheres");
                }

                String genero = generoSelecionado.getText().toString();



                PessoaObject pessoaObject =
                        new PessoaObject(Nome,Integer.parseInt(Idade),
                                genero,Descricao,Turma,Interresse);

                JsonPessoa pessoa = new JsonPessoa(pessoaObject,getApplicationContext());

                try {
                    pessoa.ConvertAndSend();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.gc();

            }
        });



    }


}
