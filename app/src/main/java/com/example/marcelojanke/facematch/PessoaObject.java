package com.example.marcelojanke.facematch;

import java.util.ArrayList;

public class PessoaObject {
    private String Nome;
    private String Genero;
    private int idade;
    private String Descricao;
    private String Turma;
    private ArrayList Interresse;

    PessoaObject(String nome, String genero, int idade,
                        String descricao, String turma, ArrayList interresse) {
        Nome = nome;
        Genero = genero;
        this.idade = idade;
        Descricao = descricao;
        Turma = turma;
        Interresse = interresse;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String nome) {
        Nome = nome;
    }

    public String getGenero() {
        return Genero;
    }

    public void setGenero(String genero) {
        Genero = genero;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public String getDescricao() {
        return Descricao;
    }

    public void setDescricao(String descricao) {
        Descricao = descricao;
    }

    public String getTurma() {
        return Turma;
    }

    public void setTurma(String turma) {
        Turma = turma;
    }

    public ArrayList getInterresse() {
        return Interresse;
    }

    public void setInterresse(ArrayList interresse) {
        Interresse = interresse;
    }
}
