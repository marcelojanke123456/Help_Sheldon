package com.example.marcelojanke.facematch.WebRequest;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SqLiteCreator extends SQLiteOpenHelper {
    SqLiteCreator(Context context) {
        super(context, "ID",null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String query = "CREATE TABLE IDCell(indent INTEGER PRIMARY KEY,ID text not null)";

        db.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void Insert(String ID){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("ident",1);
        values.put("ID",ID);

        db.insert("IDCell",null,values);
    }

    public String CheckID(String ID){
        SQLiteDatabase db = getReadableDatabase();

        String query = "SELECT ID Where ID Like ?";
        Cursor cursor = db.rawQuery(query,new String[] {ID});
        if(cursor.moveToFirst()){
            return cursor.getString(0);
        }else{
            return "Erro";
        }

    }


    public void Delete(String ID){
        SQLiteDatabase db = getWritableDatabase();

        db.delete("IDCell","ID"+ "like" +ID,null);
    }

}
